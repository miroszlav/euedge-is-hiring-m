package com.euedge.coolstuff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoolStuffShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoolStuffShopApplication.class, args);
    }
}
