*This repo contains an intro challenge for Java developers, created by EU Edge, a cool online product development studio in Budapest, Hungary - [check out more details over here](http://bit.ly/1Rrcpd5)*
## So you want to prove you're worthy to join **EU Edge**? Great!##
**Up for a little challenge? Here you go:**

1. Register an account on Bitbucket.
2. Fork this repository as a **private one**.
3. Get comfortable with our codebase. Use your favourite IDE to compile and run the project. 
4. Find the TODO comments listed below also within the project. These are the tasks that we would like you to solve.

Our challenge fits all experience level, so whether you have years behind your back or just hours, it doesn't matter. Doing the first task is compulsory and then solve as many as you wish or can from the remaining ones. In case you apply for a senior position we expect more than the intro task of course ;)

This helps us better understand your experience level and the way you think. It's ok to submit tasks that are not fully accomplished, just give us an explanation in comment why you got stuck or haven't finished it.

**The intro task...** 

```
#!Java
ProductService class:
//TODO: implement product field specific filters
```

**...and the optional, extra tasks:**

```
#!Java


ProductController class:
// TODO: introduce logging
// TODO: introduce paging and sorting(A-Z, Price, Popularity) functionality with filter
// TODO: show available products first
// TODO: introduce paging and sorting functionality with search
// TODO: introduce multi-term search functionality
// TODO: combine term search with filtering functionality
// TODO: introduce error handling
```

**When you feel ready, please let us know by:**

1. Pushing your CV into your private repository.
2. Sharing your repository with us, by adding the following user: **i.want.to.work@euedge.com**

**If you get stuck or have any questions then feel free to contact us at i.want.to.work@euedge.com**